#!/usr/bin/env python
# coding: utf-8

# In[1]:
import pandas as pd


# In[2]:

# Read in csv file, returns a dataframe df
df = pd.read_csv ('Mdata.csv')
#print (df)


# In[3]:

# Removes the column from the dataframe
# Columns: Date, Sample, Sample Number, Sample Type, test, cefo?, dilution, Count, CBT compartments positive
Cleandf= df.drop(columns=['CBT compartments positive'])
#print (Cleandf)


# In[4]:

# Returns a dataframe with all rows where 'cefo?' column is 1
# Refer to https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.loc.html
CefoDF = Cleandf.loc[Cleandf['cefo?'] == 1]


# In[5]:

# Resets index to default numerical index (starts at 0?)
# Refer to https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.reset_index.html
CefoDF = CefoDF.reset_index(drop=True)
#CefoDF


# In[6]:

#Get dataframes of all rows where test is MF, CBT, CBT2
MF_C_df = CefoDF.loc[CefoDF['test'] == 'MF'] #print(MF_C_df)
CBT_C_df = CefoDF.loc[CefoDF['test'] == 'CBT'] #print(CBT_C_df)
CBT_2_df = CefoDF.loc[CefoDF['test'] == 'CBT2'] #print(CBT_2_df)



# In[7]:

# Group MF dataframe by sample number, get the standard deviation of sample['Count'] with a delta degrees of freedom of 1
# Refer to https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.groupby.html
MF_C_SD = MF_C_df.groupby('Sample Number')['Count'].std(ddof=1).to_list() 
MF_C_Mean = MF_C_df.groupby('Sample Number')['Count'].mean().to_list()
CBT_C_SD = CBT_C_df.groupby('Sample Number')['Count'].std(ddof=1).to_list()
CBT_C_Mean = CBT_C_df.groupby('Sample Number')['Count'].mean().to_list()
CBT_2_SD = CBT_2_df.groupby('Sample Number')['Count'].std(ddof=1).to_list()
CBT_2_Mean = CBT_2_df.groupby('Sample Number')['Count'].mean().to_list()


# In[8]:


co = 0 #iteration variable
MF_C_SDS = [] #
for i in range(len(MF_C_df['Sample Number'])):
    if co+1 == MF_C_df['Sample Number'].iloc[i] :
        MF_C_SDS.append(MF_C_SD[co])
        #print(MF_C_SD[MF_C_df['Sample Number'].iloc[i]])
    else:
        co != MF_C_df['Sample Number'].iloc[i] 
        co = co+1
        MF_C_SDS.append(MF_C_SD[co])

MF_C_df.insert(13, "Standard Deviation", MF_C_SDS)

co = 0
MF_C_Ms = []
for i in range(len(MF_C_df['Sample Number'])):
    if co+1 == MF_C_df['Sample Number'].iloc[i] :
        MF_C_Ms.append(MF_C_Mean[co])
        #print(MF_C_SD[MF_C_df['Sample Number'].iloc[i]])
    else:
        co != MF_C_df['Sample Number'].iloc[i] 
        co = co+1
        MF_C_Ms.append(MF_C_Mean[co])

MF_C_df.insert(14, "Mean", MF_C_Ms)


# In[9]:


MF_C_df


# In[ ]:





# In[18]:


co = 1
Icounter = 0 
CBT_C_SDS = []
for i in range(len(CBT_C_df['Sample Number'])):
    if co == CBT_C_df['Sample Number'].iloc[i]:
        CBT_C_SDS.append(CBT_C_SD[CBT_C_df['Sample Number'].iloc[i]])
    elif co+1 == CBT_C_df['Sample Number'].iloc[i] :
        co = co+1
        CBT_C_SDS.append(CBT_C_SD[CBT_C_df['Sample Number'].iloc[i]])                
    else:
        co = co+1

CBT_C_df.insert(13, "Standard Deviation", CBT_C_SDS)


# In[ ]:



co = 0
CBT_C_Ms = []
for i in range(len(CBT_C_df['Sample Number'])):
    if co+1 == CBT_C_df['Sample Number'].iloc[i] :
        CBT_C_Ms.append(CBT_C_Mean[co])
    elif co+2 == CBT_C_df['Sample Number'].iloc[i] :
        co = co+1
        CBT_C_Ms.append(CBT_C_Mean[co])
    else: 
        co = co+1

CBT_C_df.insert(14, "Mean", CBT_C_Ms)


# In[ ]:


#Antibiotic Resistant Statistics Waste Water
CefoWWDF = CefoDF.loc[CefoDF['Sample Type'] == 'Waste Water']
ResAvgWW = CefoWWDF["Tcount"].mean()
print ('Cefo Resistant WW Average is', ResAvgWW, "CFU per 100mL")
ResMaxWW = CefoWWDF["Tcount"].max()
print ('Cefo Resistant WW maximum is', ResMaxWW, "CFU per 100 mL")
#ResVarWW = CefoWWDF["Tcount"].var()
#print ('Cefo Resistant WW variance is', ResVarWW)


# In[ ]:


#Antibiotic Resistant Statistics Surface Water
CefoSWDF = CefoDF.loc[CefoDF['Sample Type'] == 'Surface Water']
ResAvgSW = CefoSWDF["Tcount"].mean()
print ('Cefo Resistant SW average is', ResAvgSW, "CFU per 100mL")
ResMaxSW = CefoSWDF["Tcount"].max()
print ('Cefo Resistant SW maximum is', ResMaxSW, "CFU per 100mL")
#ResVarSW = CefoWWDF["Tcount"].var()
#print ('Cefo Resistant WW variance is', ResVarWW)


# In[ ]:


#Antibiotic Resistant Statistics Animal Water
CefoADF = CefoDF.loc[CefoDF['Sample Type'] == 'Animal']
ResAvgA = CefoADF["Tcount"].mean()
print ('Cefo Resistant SW average is', ResAvgA, "CFU per 100 mL")
ResMaxA = CefoADF["Tcount"].max()
print ('Cefo Resistant SW maximum is', ResMaxA, "CFU per 100 mL")
#ResVarSW = CefoWWDF["Tcount"].var()
#print ('Cefo Resistant WW variance is', ResVarWW)


# In[ ]:





# In[ ]:




