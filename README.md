# Clark Masters Research Code

Code for Clark's research, process should follow [this statistics document (WIP)](https://docs.google.com/document/d/1RKT9qSxejCO5XOEZD1b_ZMN3mWdEcMDOcl4Gc6HH1Ug/edit?usp=sharing)

## Installation

Node: In cmd, in the same file as package.lock, run the command on the following line

npm install

## Running

Node: Either use start-node.bat or via the command line node start

Python: Use start-python.bat

