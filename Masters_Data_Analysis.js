const fs = require('fs'); //filesystem operations
var ss = require('simple-statistics');
var anova = require('anova'); //ONLY USE IF ANOVA IS REQUIRED!!!--------------------------------- Not sure if this works correctly with non-equivalent sized sets ---------------------------------

const csvIn = 'Mdata.csv';

//Create blank object for data
var data = {};

//Read in csv file to array of rows (excluding the first line)
data.all = fs.readFileSync(csvIn).toLocaleString().split("\n"); //Not sure what toLocaleString() is useful for here ---------------------------------------
data.all.shift();

//change all row strings to objects with correct values
for(var i=0;i<data.all.length;i++){data.all[i] = rowParser(data.all[i],i);}
data.entries = data.all; //Standardize all data to the entries property

//Create cefo-sorted data
data.cefo = [{'entries':[]},{'entries':[]}];
for(var i=0;i<data.all.length;i++){
	if(data.all[i].cefo){
		data.cefo[1].entries.push(data.all[i]);
	}else{
		data.cefo[0].entries.push(data.all[i]);
	}
}

//Loop through cefos to generate groups
for(var i=0;i<data.cefo.length;i++){
	//Generate groups for each of the tests
	data.cefo[i].possibleValues = getSortables(data.cefo[i].entries);
	data.cefo[i].test = data.cefo[i].possibleValues.test;
	for(var j=0;j<data.cefo[i].test.length;j++){
		data.cefo[i].test[j] = {
			name: data.cefo[i].test[j],
			entries: getAllWith(data.cefo[i].entries,'test',data.cefo[i].test[j])
		};
		//Generate groups for each location
		data.cefo[i].test[j].possibleValues = getSortables(data.cefo[i].test[j].entries);
		data.cefo[i].test[j].location = data.cefo[i].test[j].possibleValues.location;
		for(var k=0;k<data.cefo[i].test[j].location.length;k++){
			data.cefo[i].test[j].location[k] = {
				name: data.cefo[i].test[j].location[k],
				entries: getAllWith(data.cefo[i].test[j].entries,'location',data.cefo[i].test[j].location[k])
			};
			//Generate groups for each DAY at each location for each test for cefo or non-cefo
			data.cefo[i].test[j].location[k].possibleValues = getSortables(data.cefo[i].test[j].location[k].entries);
			data.cefo[i].test[j].location[k].day = data.cefo[i].test[j].location[k].possibleValues.date;
			for(var l=0;l<data.cefo[i].test[j].location[k].day.length;l++){
				data.cefo[i].test[j].location[k].day[l] = {
					name: data.cefo[i].test[j].location[k].day[l],
					entries: getAllWith(data.cefo[i].test[j].location[k].entries,'date',data.cefo[i].test[j].location[k].day[l])
				};
				
			}
		}
	}
}

//1ST DATA ANALYSIS======================================================================================================================
analysis1(data.cefo[1]);//Only use cefo data only for first analysis ---------------------------------------------------------------------------------





//----------------------------------------------------------------------------------------------------------------

//gets all possible values to sort against in a group
function getSortables(group){
	return {
		location:listPossibilities("location",group),
		type:listPossibilities("type",group),
		test:listPossibilities("test",group),
		date:listPossibilities("date",group),
		sample:listPossibilities("number",group)
	};
}

//Create object constructor for row information
function rowParser(rowString,indexIn){
	let vals = rowString.split(",");
	let thisRow = {};
	thisRow.fullDate = vals[0];
	vals[0] = vals[0].split("/");
	thisRow.date = vals[0];
	//thisRow.date = {};
	//thisRow.date.month = Number(vals[0][0]);
	//thisRow.date.day = Number(vals[0][1]);
	thisRow.location = vals[1];
	thisRow.sample = {};
	thisRow.sample.number = Number(vals[2]);
	thisRow.sample.type = vals[3];
	thisRow.test = vals[4];
	thisRow.cefo = (Number(vals[5])==1); //TRUE OR FALSE
	thisRow.dilution = (vals[6]=="u" ? 0 : Number(vals[6]));
	thisRow.count = Number(vals[7]);
	thisRow.index = indexIn;
	return thisRow;
}

//Get index of aan object with a name
function indexByName(arr,name){
	for(var i=0;i<arr.length;i++){
		if(arr[i].name == name){return i;}
	}return false;
}

//Function to list all possible values in a column for a group of entries
function listPossibilities(sortBy,group){
	let temp = [];
	var toTest;
	switch(sortBy){
		case "location": toTest = "group[i].location"; break; //Sample location
		case "type":     toTest = "group[i].sample.type"; break;//Sample type
		case "number":   toTest = "group[i].sample.number"; break;//Sample number
		case "test":     toTest = "group[i].test"; break;//Test type
		case "date":     toTest = "group[i].fullDate"; break;//Date
		case "cefo":     toTest = "group[i].cefo"; break;//cefo
		case "dilution": toTest = "group[i].dilution"; break;//Dilution
		default: console.log("ERROR: Defaulted on sortBy in listPossibilities function"); return [];
	}
	for(var i=0;i<group.length;i++){
		let testValue = eval(toTest);
		if(!(temp.includes(testValue))){temp.push(testValue);}
	}
	if(temp.length == 0){console.log("Warning: listPossibilities returned 0 entries");}
	return temp;
}

//Function to list all entries in a column with a specific value, for a group of entries
function getAllWith(group,attribute,value){
	let output = [];
	var toTest;
	switch(attribute){
		case "location": toTest = "group[i].location"; break; //Sample location
		case "type":     toTest = "group[i].sample.type"; break;//Sample type
		case "number":   toTest = "group[i].sample.number"; break;//Sample number
		case "test":     toTest = "group[i].test"; break;//Test type
		case "date":     toTest = "group[i].fullDate"; break;//Date
		case "cefo":     toTest = "group[i].cefo"; break;//cefo
		case "dilution": toTest = "group[i].dilution"; break;//Dilution
		default: console.log("ERROR: Defaulted on attribute in getAllWith function"); return [];
	}
	for(var i=0;i<group.length;i++){ if(eval(toTest)==value){ output.push(group[i]);}}
	if(output.length == 0){console.log("Warning: getAllWith returned 0 entries");}//Warn if no returns
	return output;
}

//Average of a list of numbers
function mean(nums){
	let toOut = 0;
	for(var i=0;i<nums.length;i++){
		toOut=toOut+nums[i];
	}
	return toOut/(nums.length);
}

//Staandard deviation for a group (array) of numbers
function stdev(nums){
	let toOut = 0;
	let mu = mean(nums);
	for(var i=0;i<nums.length;i++){
		toOut = toOut+((nums[i]-mu)**2);
	}return Math.sqrt(toOut/nums.length);
}

//Function to perform a power transform on an entire group of numbers
function power_transform_group(ys,lambda){// -------------------------------------------------UNUSED----------------------------------
	let GM = geometric_mean(y_values);
	let power_transforms = [];
	for(var i=0;i<ys.length;i++){
		power_transforms.push(power_transform(ys[i],GM,lambda));
	}return power_transforms
}

//Function to perform a power transform on a single number
function power_transform(y,GM,lambda){// -------------------------------------------------UNUSED----------------------------------
	if(lambda==0){return (GM*Math.log(y));}
	return ((Math.pow(y,lambda)-1)/(lambda*Math.pow(GM,(lambda-1))));
}

//Power transform for a singular number
function geometric_mean(ys){// -------------------------------------------------UNUSED----------------------------------
	let GM = 1;
	for(var i=0;i<ys.length;i++){
		GM = GM*ys[i];
	}
	return Math.pow(GM,(1/ys.length));
}

//-----------------------------------------------------------------------------
//Iterative methods which may be necessary for finding roots of transcendental functions (e.g. lambda for power transform)
//Directive function for iterative functions
async function iterate(eqOneIn,eqTwoIn,bounds,method,acc){// -------------------------------------------------UNUSED----------------------------------
	//set temporary function variables
	let eqOne = eqOneIn; let eqTwo = eqTwoIn;
	if(typeof eqOne!="function"){eqOne=function(x){return eqOneIn;};}//creates an arbitrary function if eq1 is a number instead of a function
	if(typeof eqTwo!="function"){eqTwo=function(x){return eqTwoIn;};}//creates an arbitrary function if eq2 is a number instead of a function
	switch(method){
		case "falsePosition": return falsePositionMethod(eqOne,eqTwo,bounds,acc); break;
		case "bisection": return bisectionMethod(eqOne,eqTwo,bounds,acc); break;
		default:
			console.log("Method "+method+" not recognized for iteration, falling back to bisection method!");
			return bisectionMethod(eqOne,eqTwo,bounds,acc);
	}
}

//False position method of finding roots in transcendental equations
function falsePositionMethod(fx1,fx2,bounds,acc){// -------------------------------------------------UNUSED----------------------------------
	return new Promise((accept,reject)=>{
		let fxn3 = (x)=>{return (fx1(x)-fx2(x));}; /* Compound function = fx3 */
		let bound = bounds; let lastxn=0;
		while(true){ let toGo = bound;
			let xn = toGo[0]-(((toGo[1]-toGo[0])*fxn3(toGo[0]))/(fxn3(toGo[1])-fxn3(toGo[0])));
			if(Math.abs(xn-lastxn)<acc){accept(xn);break;}
			if(fxn3(xn)==0){accept(xn);break;}
			if(Math.sign(fxn3(xn))==NaN){reject('ERROR! ITERATION YIELDED NaN!');}
			if(fxn3(xn)*fxn3(toGo[0])>0){bound[0]=xn;}else{bound[1]=xn;}
			if(Math.abs(bound[0]-bound[1])<acc){accept(xn);break;}
			lastxn = xn;
		} reject(falsePosition);
	});
}

//Bisection method of finding roots in transcendental equations
function bisectionMethod(fxn1,fxn2,bounds,acc){// -------------------------------------------------UNUSED----------------------------------
	return new Promise((accept,reject)=>{
		let bound=bounds;
		let lastxn=bounds[0];
		let fxn3 = (x)=>{return (fxn1(x)-fxn2(x));};
		while(true){ let midPt = (bound[0]+bound[1])/2;
			if(fxn3(midPt)==0||(Math.abs(midPt-bound[0])<acc&&Math.abs(midPt-bound[1])<acc)){ accept(midPt); break; }
			else{ lastxn=midPt;
				if(Math.sign(fxn3(bound[0]))==Math.sign(fxn3(midPt))){bound[0]=midPt;}else{
					if(Math.sign(fxn3(bound[1]))==Math.sign(fxn3(midPt))){bound[1]=midPt;}else{
						reject('ERROR! BISECTION ITERATION YIELDED NaN!'); break;
					}
				}
			}
		}
	});
}

function avgGroupCount(entriesGroup){
	let a=0;
	for(var i=0;i<entriesGroup.length;i++){
		a = a+entriesGroup[i].count;
	}
	return (a/entriesGroup.length);
}

function complexSortGroup(group,sortArr){ //sortArr = [["cefo",false],["location","OWASA"],...,["date","6-Jul"]]
	let currentGroup = (group.hasOwnProperty("entries") ? group.entries : group);
	for(var i=0;i<sortArr.length;i++){
		currentGroup = getAllWith(currentGroup,sortArr[i][0],sortArr[i][1]);
		if(currentGroup.length == 0){console.log("Error when looking for ",sortArr[i]);}
		}
	return currentGroup;
}

function analysis1(analysisData){
	//analysisData.test[j].location[k].day[m].entries
	//Comparisons: MF-CBT, MF-CBT2
	let sampleNumbers = listPossibilities("number",analysisData.entries);
	let sample = [];
	for(var i=0;i<sampleNumbers.length;i++){ //For each sample number, create an object in sample
		sample.push({number:sampleNumbers[i]});
		//Check which tests this sample used
		sample[i].has = {
			MF: listPossibilities("number",analysisData.test[0].entries).includes(sample[i].number),
			CBT: listPossibilities("number",analysisData.test[1].entries).includes(sample[i].number),
			CBT2: listPossibilities("number",analysisData.test[2].entries).includes(sample[i].number)
		};
		//Create arrays to hold samples counts and means data
		sample[i].counts = {}; sample[i].mean = {};
		sample[i].counts.MF = [];
		sample[i].counts.CBT = [];
		sample[i].counts.CBT2 = [];
		
		//Get counts for the samples (sample[i].counts.TEST) and their means (sample[i].mean.TEST)
		if(sample[i].has.MF){
			let tempCounts = getAllWith(analysisData.test[0].entries,"number",sample[i].number);
			for(var j=0;j<tempCounts.length;j++){tempCounts[j] = tempCounts[j].count;}
			sample[i].counts.MF = tempCounts;
			sample[i].mean.MF = mean(tempCounts);
		}
		if(sample[i].has.CBT){
			let tempCounts = getAllWith(analysisData.test[1].entries,"number",sample[i].number);
			for(var j=0;j<tempCounts.length;j++){tempCounts[j] = tempCounts[j].count;}
			sample[i].counts.CBT = tempCounts;
			sample[i].mean.CBT = mean(tempCounts);
		}
		if(sample[i].has.CBT2){
			let tempCounts = getAllWith(analysisData.test[2].entries,"number",sample[i].number);
			for(var j=0;j<tempCounts.length;j++){tempCounts[j] = tempCounts[j].count;}
			sample[i].counts.CBT2 = tempCounts;
			sample[i].mean.CBT2 = mean(tempCounts);
		}
	}
	//Paired Samples T-test
	let counts = [0,0]; let nCounts = [[[],[]],[[],[]]];
	for(var i=0;i<sample.length;i++){ //Get differences and counts
		sample[i].diff = [0,0];
		if(sample[i].has.MF){
			if(sample[i].has.CBT){
				sample[i].diff[0] = sample[i].mean.MF + sample[i].mean.CBT;
				nCounts[0][0].push(sample[i].mean.MF);
				nCounts[0][1].push(sample[i].mean.CBT);
				counts[0]++;
			}
			if(sample[i].has.CBT2){
				sample[i].diff[1] = sample[i].mean.MF + sample[i].mean.CBT2;
				nCounts[1][0].push(sample[i].mean.MF);
				nCounts[1][1].push(sample[i].mean.CBT2);
				counts[1]++;
			}
		}
	}
	// -----------------------------------A LOT OF THE CODE IN THIS FUNCTION AABOVE HERE IS NOT NECESSARY, PROBABLY GO BACK THROUGH HERE TO OPTIMIZE LATER --------------------------------
	let tScore = [
		ss.tTestTwoSample(nCounts[0][0], nCounts[0][1] /*, difference */),
		ss.tTestTwoSample(nCounts[1][0], nCounts[1][1] /*, difference */)
	];
	
	console.log("Analysis 1 outputs (T-Values):");
	console.log("MF vs. CBT  T-value: "+tScore[0]+", df: "+(counts[0]-1));
	console.log("MF vs. CBT2 T-value: "+tScore[1]+", df: "+(counts[1]-1));
	
	//https://jstat.github.io/all.html#jStat.studentt
}

console.log();
console.log();
console.log();
// REFERENCE AREA======================================================
console.log("Reference Area below, probably don't pay attention to anything below here");

var sampless = [[3,3,5,1], [1,2,3], [1,2,3]];
console.log("ANOVA test: "+anova.test(sampless)); // 0.8571428571428572

//data.cefo[i].test[j].location[k].day[l]

let exampleGroup = data.entries;

//Get average count of surface water sites, only cefo
console.log("Get average count of surface water sites, only cefo");
let ref = data.cefo[1]; //Only cefo data
ref = getAllWith(ref.entries,"type","Surface Water"); //Reduce down to only entries with surface water as the sample type
ref = avgGroupCount(ref.entries); //Get average count of all referenced entries

//List possible dates within the group
ref = listPossibilities("date",exampleGroup); 

//Get all entries in a group for a specific day
console.log("Getting all entries in a group for a specific day");
ref = getAllWith(exampleGroup,"date","6-Jul").entries;


//Get all MF at RL on 12-Oct
console.log("Get all MF at RL on 12-Oct");
ref = {cefo:[0,0]};
ref.cefo[0] = complexSortGroup(data,[["cefo",false],["test","MF"],["location","RL"],["date","12-Oct"]/*,["dilution",-4]*/]); //RETURNS AN ARRAY OF ENTRIES
ref.cefo[1] = complexSortGroup(data,[["cefo",true],["test","MF"],["location","RL"],["date","12-Oct"]]);
//First entry of ref.cefo[0] is ref.cefo[0][0]

//Getting an entry within a group
//group.entry[1]; //Gets second entry of group (1st is 0, 3rd is 2)

//Getting properties of an entry
/*
exampleGroup.entries[0].PROPERTY
Available properties:
.fullDate
.date
.location
.sample.number = Number(vals[2]);
.sample.type = vals[3];
.test = vals[4];
.cefo = (Number(vals[5])==1); //TRUE OR FALSE
.dilution = Number(vals[6]);
.count = Number(vals[7]);
.index = indexIn;
*/



