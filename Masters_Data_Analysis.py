#!/usr/bin/env python
# coding: utf-8

# In[1]:
import pandas as pd
import numpy as np
import math
from sklearn.preprocessing import PowerTransformer

# In[2]:

# function to get unique values
def unique(list1):
    unique_list = [] # initialize a null list
    for x in list1: # traverse for all elements
        if x not in unique_list: # check if exists in unique_list or not
            unique_list.append(x)
    return unique_list # return unique list
	
# function to get unique values for Date
def uniqueDay(list1):
    unique_list = [] # initialize a null list
    for x in list1:  # traverse for all elements
        if x["Date"] not in unique_list: # check if exists in unique_list or not
            unique_list.append(x["Date"])
    return unique_list # return unique list

# Read in csv file, returns a dataframe df
df = pd.read_csv ('Mdata.csv')

# In[3]:

# Removes the column from the dataframe
Cleandf= df.drop(columns=['CBT compartments positive'])

# In[4]:

# Get dataframes with all rows where 'cefo?' column is 0 or 1
#CefoDF = Cleandf.loc[Cleandf['cefo?'] == 1]
data = [{},{}] # data[0]=cefo, data[1]=non-cefo
data[0]["all"] = Cleandf.loc[Cleandf['cefo?'] == 0]
data[1]["all"] = Cleandf.loc[Cleandf['cefo?'] == 1]


# In[5]:
testTypes = ["MF","CBT","CBT2"]
Columns = ["Date", "Sample", "Sample Number", "Sample Type", "test", "cefo?", "dilution", "Count"] #+ "CBT compartments positive"

#Format data into a useable manner
for cefo in data:
	# Refer to https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.loc.html
	#cefo.all = Cleandf.loc[Cleandf['cefo?'] == index]
	# Resets index to default numerical index (starts at 0)
	# Refer to https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.reset_index.html
	cefo["all"] = cefo["all"].reset_index(drop=True)
	#Get dataframes of all rows where test is MF, CBT, CBT2
	cefo["original"] = cefo["all"]
	#For each test type
	for dType in testTypes:
		cefo[dType] = cefo["all"].loc[cefo["all"]['test'] == dType].to_dict()
		#Convert data into arrays at the base level to rid the need of key indices
		for key in cefo[dType]:
			cefo[dType][key] = list(cefo[dType][key].values())
		#Get all possible locations "Sample"s
		cefo[dType]["locations"] = unique(cefo[dType]["Sample"]) #UNTESTED ---------------------------------------------------------------
		cefo[dType]["loc"] = {}
		#For each location, create a key and import all applicable data
		for loc in cefo[dType]["locations"]:
			cefo[dType]["loc"][loc] = {}
			cefo[dType]["loc"][loc]["byDate"] = {}
			cefo[dType]["loc"][loc]["data"] = []
			for i in range(len(cefo[dType]["Sample"])):
				if cefo[dType]["Sample"][i] == loc:
					tempData = {}
					tempData["Date"] = cefo[dType]["Date"][i]
					tempData["Sample Number"] = cefo[dType]["Sample Number"][i]
					tempData["Sample Type"] = cefo[dType]["Sample Type"][i]
					tempData["dilution"] = cefo[dType]["dilution"][i]
					tempData["Count"] = cefo[dType]["Count"][i]
					cefo[dType]["loc"][loc]["data"].append(tempData)
			cefo[dType]["loc"][loc]["byDate"]["list"] = uniqueDay(cefo[dType]["loc"][loc]["data"])
			cefo[dType]["loc"][loc]["byDate"]["dates"] = {}
			# For each day (in each area), compile list of all datapoints
			for day in cefo[dType]["loc"][loc]["byDate"]["list"]:
				cefo[dType]["loc"][loc]["byDate"]["dates"][day] = []
				for i in range(len(cefo[dType]["loc"][loc]["data"])):
					if cefo[dType]["loc"][loc]["data"][i]["Date"] == day:
						cefo[dType]["loc"][loc]["byDate"]["dates"][day].append(cefo[dType]["loc"][loc]["data"][i])
		
	#print (cefo["MF"]["Count"][2])
	#print (cefo["CBT"])
	#USAGE OF STUFF -----------------------------------------------------   data[0/1][testType]["loc"][key]
	# data[0/1][testType]["loc"][areaKey]["data"][n]["Date/dilution/count"]
	# data[0/1][testType]["loc"][areaKey]["byDate"][dayString][n]["Date/dilution/count"]
	
	
# 	------------------------------------------------------------Start first Data analysis --------------------------------------------------------------------------

# End of first data analysis
	
#Test power transformer --------------------------------------------------------------------------------------------------
pt = PowerTransformer()#method="box-cox" #Not working
test = [[1, 2, 7, 8], [3, 2, 5, 5], [4, 5, 1, 0]]
print(pt.fit(test))
#PowerTransformer()
print('lambdas:___________')
print(pt.lambdas_)
print('transform:___________')
print(pt.transform(test))
print('----------------------------')

pt = PowerTransformer()
test = [[1], [2], [7], [3]]
print(pt.fit(test))
#PowerTransformer()
print('lambdas:___________')
print(pt.lambdas_)
print('transform:___________')
print(pt.transform(test))
print('----------------------------')

#Average of a list of numbers
def mean(numsList):
	toOut = 0
	for n in numsList:
		toOut = toOut + n
	return toOut/len(numsList)

#Standard deviation of a list of numbers
def stdev(numsList):
	toOut = 0
	mu = mean(numsList)
	for n in numsList:
		toOut = toOut + ((n - mu) ** 2)
	return math.sqrt(toOut/len(numsList))

#Function to perform a power transform on an entire group of numbers
def power_transform_group(y_values,lmbda):
	GM = geometric_mean(y_values)
	power_transforms = []
	for y in y_values:
		power_transforms.append(power_transform(y,GM,lmbda))
	return power_transforms

#Power transform for a singular number
def power_transform(y,GM,lmbda):
	if lmbda == 0:
		return GM*np.log(y)
	else:
		return ((y**lmbda)-1)/(lmbda*(GM**(lmbda-1)))

#Geometric mean of a group of numbers
def geometric_mean(y_values):
	GM = 1
	for y in y_values:
    		GM = GM * y
	return GM**(1/len(y_values))
	
# NEEDS--------------------------------------------------------------------------------------------
# Function to find optimal lambda

# For now ignore all non-cefo data

# Data analysis steps
# Compare performance of CBT and CBT2 vs Membrane Filtration

# Means comparison test (will result in non-stat-significant results)
# Each sample for each test and compare it to the same sample for other tests (compare means of samples of same day of same test at same place)
# Temporal doesnt matter comparatively
# Google T-test
# Probably wont produce statistically significant results

# ANOVA
# Analysis of variance
# Finding Z-score: f statistic and p-value

# Paired ANOVA

# End Goals------------------
# Compare performance of CBT and CBT2 vs Membrane Filtration


# In[6]:



# In[7]:
